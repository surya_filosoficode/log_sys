var express = require("express");
var app = express();
var port = 3000;

// var mongoose = require("mongoose");
// mongoose.Promise = global.Promise;mongoose.connect("mongodb://localhost:27017/node-demo");

const { MongoClient } = require("mongodb");
// Replace the uri string with your MongoDB deployment's connection string.
const uri = "mongodb://localhost:27017";
const client = new MongoClient(uri);
async function run() {
  try {
    await client.connect();
    const database = client.db("log_test");
    const collection = database.collection("test_aja");
    // create a document to be inserted

    let ts = Date.now();

    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();

    let h = date_ob.getHours();
    let m = date_ob.getMinutes();
    let s = date_ob.getSeconds();

    // prints date & time in YYYY-MM-DD format
    // console.log(year + "-" + month + "-" + date);
    var date_fix = year + "-" + month + "-" + date + " " + h + ":" + m + ":" + s;

    const doc = { date: date_fix, town: "test_aja" };
    const result = await collection.insertOne(doc);
    console.log(
      `${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`,
    );
  } finally {
    await client.close();
  }
}
run().catch(console.dir);

// app.get("/", (req, res) => {
//  res.send("Hello World");
// });
 
// app.listen(port, () => {
//  console.log("Server listening on port " + port);
// });